<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 23-Feb-19
 * Time: 03:38
 */

namespace App\Response;


class CustomerResponse implements \JsonSerializable
{
    private $response;

    /**
     * CustomerResponse constructor.
     * @param $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
