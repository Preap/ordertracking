<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 16:26
 */

namespace App\Response;


class HomepageResponse implements \JsonSerializable
{
    private $message;

    /**
     * HomepageResponse constructor.
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
