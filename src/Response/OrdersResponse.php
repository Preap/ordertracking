<?php
/**
 * Created by PhpStorm.
 * User: CUTing Edge
 * Date: 04/04/2019
 * Time: 11:21 AM
 */

namespace App\Response;


class OrdersResponse implements \JsonSerializable
{
    private $response;

    /**
     * AddressResponse constructor.
     * @param $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
