<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 16:16
 */

namespace App\Request;


use Symfony\Component\HttpFoundation\Request;

class HomepageRequest
{
    public $name;
    public $age;
    public $description;

    /**
     * HomepageRequest constructor.
     * @param $name
     * @param $age
     * @param $description
     */
    public function __construct($name, $age, $description)
    {
        $this->name = $name;
        $this->age = $age;
        $this->description = $description;
    }

    public static function fromRequest (Request $request){
        $data = $request->getContent();
        $array = json_decode($data,true);
        $name = $array["name"];
        $age = $array["age"];
        $description = $array["description"];

        return new self($name, $age, $description);
    }
}
