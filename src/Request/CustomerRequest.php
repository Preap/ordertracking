<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 23-Feb-19
 * Time: 03:38
 */

namespace App\Request;


use Symfony\Component\HttpFoundation\Request;

class CustomerRequest
{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;

    /**
     * CustomerRequest constructor.
     * @param $first_name
     * @param $last_name
     * @param $email
     * @param $phone
     * @throws \Exception
     */
    public function __construct($first_name, $last_name, $email, $phone)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->phone = $phone;
    }

    public static function insertRequest(Request $request){
        $data = $request->getContent();
        $array = json_decode($data,true);

        if(!isset($array["first_name"]))
        { throw new \Exception("First name not set", 400);}
        if(!isset($array["last_name"]))
        { throw new \Exception("Last name not set", 400);}
        if(!isset($array["phone"]))
        { throw new \Exception("Phone not set", 400);}
        if(!isset($array["email"]))
        { throw new \Exception("E-mail not set", 400);}

        $last_name = $array["last_name"];
        $first_name = $array["first_name"];
        $phone = $array["phone"];
        $email = $array["email"];

        return new self($first_name,$last_name,$email,$phone);
    }
}
