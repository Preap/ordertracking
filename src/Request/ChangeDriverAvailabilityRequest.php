<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 29-May-19
 * Time: 16:20
 */

namespace App\Request;


use Symfony\Component\HttpFoundation\Request;

class ChangeDriverAvailabilityRequest
{
    public $driver_id;
    public $availability;

    /**
     * ChangeDriverAvailabilityRequest constructor.
     * @param $driver_id
     * @param $availability
     */
    public function __construct($driver_id, $availability)
    {
        $this->driver_id = $driver_id;
        $this->availability = $availability;
    }

    public static function changeDriverAvailability(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data, true);

        if(!isset($array["driver_id"]))
        {throw new \Exception("driver_id not set!", 400);}
        if(!isset($array["availability"]))
        {throw new \Exception("Availability not set!", 400);}

        $driver_id = $array["driver_id"];
        $availability = $array["availability"];

        return new self($driver_id, $availability);
    }


}
