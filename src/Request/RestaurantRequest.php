<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 26-Feb-19
 * Time: 17:24
 */

namespace App\Request;


use Symfony\Component\HttpFoundation\Request;

class RestaurantRequest
{
    public $address_id;
    public $name;
    public $phone;
    public $password;

    /**
     * RestaurantRequest constructor.
     * @param $address_id
     * @param $name
     * @param $phone
     * @param $password
     */
    public function __construct($address_id, $name, $phone, $password)
    {
        $this->address_id = $address_id;
        $this->name = $name;
        $this->phone = $phone;
        $this->password = $password;
    }

    /**
     * @param Request $request
     * @return RestaurantRequest
     * @throws \Exception
     */
    public static function insertRequest(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data,true);

        if(!isset($array["address_id"]))
        {  throw new \Exception("Restaurant AddressID not set",400);}
        if(!isset($array["name"]))
        {  throw new \Exception("Restaurant name not set",400);}
        if(!isset($array["phone"]))
        {  throw new \Exception("Restaurant phone not set",400);}
        if(!isset($array["password"]))
        {  throw new \Exception("Restaurant password not set",400);}

        $address_id = $array["address_id"];
        $name = $array["name"];
        $phone = $array["phone"];
        $password = $array["password"];

        return new self($address_id,$name,$phone,$password);
    }
}
