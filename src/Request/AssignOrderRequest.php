<?php
/**
 * Created by PhpStorm.
 * User: CUTing Edge
 * Date: 04/04/2019
 * Time: 10:55 AM
 */

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

class AssignOrderRequest
{

    public $order_id;
    public $vehicle_id;
    public $driver_id;

    /**
     * AssignOrderRequest constructor.
     * @param $order_id
     * @param $vehicle_id
     * @param $driver_id
     */
    public function __construct($order_id, $vehicle_id, $driver_id)
    {
        $this->order_id = $order_id;
        $this->vehicle_id = $vehicle_id;
        $this->driver_id = $driver_id;
    }

    /**
     * @param Request $request
     * @return AssignOrderRequest
     * @throws \Exception
     */
    public static function assignOrderRequest(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data, true);

        if(!isset($array["order_id"]))
        { throw new \Exception("Order ID not set", 400);}
        if(!isset($array["vehicle_id"]))
        { throw new \Exception("Vehicle ID not set", 400);}
        if(!isset($array["driver_id"]))
        { throw new \Exception("Driver ID not set", 400);}

        $driver_id = $array["driver_id"];
        $vehicle_id = $array["vehicle_id"];
        $order_id = $array["order_id"];

        return new self($order_id,$vehicle_id,$driver_id);
    }
}
