<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 17:53
 */

namespace App\Request;


use Symfony\Component\HttpFoundation\Request;

class AddressRequest
{
    public $number;
    public $street_name;
    public $post_code;
    public $area;
    public $latitude;
    public $longitude;

    /**
     * AddressRequest constructor.
     * @param $number
     * @param $street_name
     * @param $post_code
     * @param $area
     * @param $latitude
     * @param $longitude
     */
    public function __construct($number, $street_name, $post_code, $area, $latitude, $longitude)
    {
        $this->number = $number;
        $this->street_name = $street_name;
        $this->post_code = $post_code;
        $this->area = $area;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @param Request $request
     * @return AddressRequest
     * @throws \Exception
     */
    public static function insertRequest(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data,true);

        if(!isset($array["number"]))
        { throw new \Exception("Address number not set",400);}
        if(!isset($array["street_name"]))
        { throw new \Exception("Street Name not set",400);}
        if(!isset($array["post_code"]))
        { throw new \Exception("Post-code not set",400);}
        if(!isset($array["area"]))
        { throw new \Exception("Area not set",400);}
        if(!isset($array["latitude"]))
        { throw new \Exception("Latitude not set",400);}
        if(!isset($array["longitude"]))
        { throw new \Exception("Longitude not set",400);}

        $number = $array["number"];
        $street_name = $array["street_name"];
        $post_code = $array["post_code"];
        $area = $array["area"];
        $latitude = $array["latitude"];
        $longitude = $array["longitude"];

        return new self($number,$street_name,$post_code,$area,$latitude,$longitude);
    }
}
