<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Apr-19
 * Time: 16:24
 */

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

class ChangeOrderStatusRequest
{

    public $order_id;
    public $status;

    /**
     * ChangeOrderStatusRequest constructor.
     * @param $order_id
     * @param $status
     */
    public function __construct($order_id, $status)
    {
        $this->order_id = $order_id;
        $this->status = $status;
    }

    public static function changeOrderStatus(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data,true);

        if(!isset($array["order_id"]))
        { throw new \Exception("Order ID not set", 400);}
        if(!isset($array["status"]))
        { throw new \Exception("Order status not set", 400);}

        $order_id = $array["order_id"];
        $status = $array["status"];

        return new self($order_id,$status);
    }

}
