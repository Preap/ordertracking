<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Apr-19
 * Time: 13:19
 */

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

class CheckDriverCredentialsRequest
{
    public $phone;
    public $password;

    /**
     * CheckDriverCredentialsRequest constructor.
     * @param $phone
     * @param $password
     */
    public function __construct($phone, $password)
    {
        $this->phone = $phone;
        $this->password = $password;
    }

    /**
     * @param Request $request
     * @return CheckDriverCredentialsRequest
     * @throws \Exception
     */
    public static function checkCredentialsRequest(Request $request)
    {
        $data = $request->getContent();
        $array = json_decode($data,true);

        if(!isset($array["phone"]))
        { throw new \Exception("Phone not set!", 400);}
        if(!isset($array["password"]))
        { throw new \Exception("Password not set!", 400);}

        $phone = $array["phone"];
        $password = $array["password"];

        return new self($phone,$password);
    }
}
