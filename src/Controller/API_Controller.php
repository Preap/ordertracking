<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 2/7/2019
 * Time: 9:54 PM
 */

namespace App\Controller;


//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Customer;
use App\Request\AddressRequest;
use App\Request\AssignOrderRequest;
use App\Request\ChangeDriverAvailabilityRequest;
use App\Request\ChangeOrderStatusRequest;
use App\Request\CheckDriverCredentialsRequest;
use App\Request\CustomerRequest;
use App\Request\HomepageRequest;
use App\Response\CustomerResponse;
use App\Service\AddressService;
use App\Service\CustomerService;
use App\Service\DriverService;
use App\Service\HomepageService;
use App\Service\OrderService;
use App\Service\OrdersService;
use App\Service\RestaurantService;
use App\Service\VehicleService;
use Doctrine\ORM\EntityManagerInterface;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

//const SERVER_URL = "http://localhost:4200";
const SERVER_URL = "*";

class API_Controller extends AbstractController
{
    private function setHeaders(JsonResponse $r){
//        $r->headers->set("Access-Control-Allow-Origin", SERVER_URL);
//        $r->headers->set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
//        $r->headers->set('Access-Control-Allow-Headers', 'Host, Origin, Content-Type, Content-Length');
    }

    /**
     * @Route("/", methods={"POST", "OPTIONS"})
     */
    public function homepage()
    {
        $request = Request::createFromGlobals();
        $requestData = HomepageRequest::fromRequest($request);
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new HomepageService($entityManager);
        $responseData = $service->homepage($requestData);
        $json = new JsonResponse($responseData);
        $this->setHeaders($json);
        return $json;
    }

    /**
     * @Route("/address/insert", methods={"POST"})
     */
    public function insertAddress()
    {
        try {
            $request = Request::createFromGlobals();
            $requestedAddress = AddressRequest::insertRequest($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new AddressService($entityManager);
            $responseData = $service->insertAddress($requestedAddress);
            $response = new JsonResponse($responseData->jsonSerialize());
        }catch (\Exception $e ){
            $response = new JsonResponse($e->getMessage(),$e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/address/get/{id}", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getAddress($id){
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new AddressService($entityManager);
        try {
            $responseData = $service->getAddress($id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(),$e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/customer/insert", methods={"POST"})
     */
    public function insertCustomer(){
        try {
            $request = Request::createFromGlobals();
            $requestedCustomer = CustomerRequest::insertRequest($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new CustomerService($entityManager);
            $responseData = $service->insertCustomer($requestedCustomer);
            $response = new JsonResponse($responseData->jsonSerialize());
        } catch (\Exception $e) {
            $response = new JsonResponse($e->getMessage(),$e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("customer/get/{id}", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getCustomer($id) {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new CustomerService($entityManager);
        try {
            $responseData = $service->getCustomer($id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(),$e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("driver/getDrivers/{r_id}" ,methods={"GET"})
     * @param $r_id ==> RestaurantID
     * @return JsonResponse
     */
    public function getDrivers($r_id){
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new DriverService($entityManager);
        try{
            $responseData = $service->getDrivers($r_id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);;
        return $response;
    }

    /**
     * @Route("vehicle/getVehicles/{r_id}" ,methods={"GET"})
     * @param $r_id ==> RestaurantID
     * @return JsonResponse
     */
    public function getVehicles($r_id){
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new VehicleService($entityManager);
        try{
            $responseData = $service->getVehicles($r_id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("order/getOrders/{r_id}" ,methods={"GET"})
     * @param $r_id
     * @return JsonResponse
     */
    public function getOrders($r_id){
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new OrdersService($entityManager);
        try{
            $responseData = $service->getOrders($r_id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/restaurant/get/{r_id}" ,methods={"GET"})
     * @param $r_id
     * @return JsonResponse
     */
    public function getRestaurantById($r_id){
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $service = new RestaurantService($entityManager);
        try{
            $responseData = $service->getRestaurantById($r_id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(),$e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/order/assignDriverAndVehicle" ,methods={"POST"})
     */
    public function assignDriverAndVehicleToOrder()
    {
        try{
            $request = Request::createFromGlobals();
            $requestedAssignment = AssignOrderRequest::assignOrderRequest($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new OrdersService($entityManager);
            $responseData = $service->assignOrder($requestedAssignment);
            $response = new JsonResponse($responseData->jsonSerialize());
        } catch (\Exception $e) {
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/order/getDeliveryList/{driver_id}" ,methods={"GET"})
     */
    public function  getDeliveryList($driver_id) {
        try {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new OrdersService($entityManager);
            $responseData = $service->getDeliveryList($driver_id);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e) {
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
            $this->setHeaders($response);
            return $response;
        }

    /**
     * @Route("/driver/checkCredentials" ,methods={"POST"})
     */
    public function checkDriverCredentials() {
        try {
            $request = Request::createFromGlobals();
            $requestedCheck = CheckDriverCredentialsRequest::checkCredentialsRequest($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new DriverService($entityManager);
            $responseData = $service->checkCredentials($requestedCheck);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/order/changeStatus" ,methods={"POST"})
     */
    public function  changeOrderStatus() {
        try {
            $request = Request::createFromGlobals();
            $requestedStatus = ChangeOrderStatusRequest::changeOrderStatus($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new OrdersService($entityManager);
            $responseData = $service->changeOrderStatus($requestedStatus);
            $response = new JsonResponse($responseData);
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/getDriver/{driver_id}" ,methods={"GET"})
     * @param $driver_id
     * @return JsonResponse
     */
    public  function getDriver($driver_id) {
        try {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new DriverService($entityManager);
            $responseData = $service->getDriver($driver_id);
            $response = new JsonResponse($responseData);
    } catch (\Exception $e) {
        $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }

    /**
     * @Route("/driver/changeAvailability", methods={"POST"})
     */
    public function changeDriverAvailability(){
        try {
            $request = Request::createFromGlobals();
            $requestedAvailabilityChange = ChangeDriverAvailabilityRequest::changeDriverAvailability($request);
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $service = new DriverService($entityManager);
            $responseData = $service->changeDriverAvailability($requestedAvailabilityChange);
            $response = new JsonResponse($responseData->jsonSerialize());
        }catch (\Exception $e){
            $response = new JsonResponse($e->getMessage(), $e->getCode());
        }
        $this->setHeaders($response);
        return $response;
    }
}
