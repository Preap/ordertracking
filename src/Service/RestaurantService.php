<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 26-Feb-19
 * Time: 17:53
 */

namespace App\Service;


use App\Entity\Address;
use App\Entity\Restaurant;
use App\Request\RestaurantRequest;
use App\Response\RestaurantResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class RestaurantService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param RestaurantRequest $requestedRestaurant
     * @return RestaurantResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function insertRestaurant(RestaurantRequest $requestedRestaurant)
    {
        $restaurant = new Restaurant();

        $restaurant->setAddressID($requestedRestaurant->address_id);
        $restaurant->setName($requestedRestaurant->name);
        $restaurant->setPhone($requestedRestaurant->phone);
        $restaurant->setPassword($requestedRestaurant->password);

        $this->entityManager->persist($restaurant);
        $this->entityManager->flush();

        $responseMessage = "Successfully added new restaurant with ID: "
            .$restaurant->getId()." to the database!";
        return new RestaurantResponse($responseMessage);
    }

    /**
     * @param $r_id
     * @return object|null
     * @throws \Exception
     */
    public function getRestaurantById($r_id){
        $restaurant = $this->entityManager
            ->getRepository(Restaurant::class)
            ->find($r_id);

        if(!$restaurant){
            throw new \Exception("Restaurant not found", 404);
        }
        return $restaurant;
    }
}
