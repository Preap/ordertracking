<?php
/**
 * Created by PhpStorm.
 * User: CUTing Edge
 * Date: 12/03/2019
 * Time: 1:41 PM
 */

namespace App\Service;


use App\Entity\Order;
use App\Entity\Vehicle;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $r_id
     * @return array|object[]
     * @throws \Exception
     */
    public function getOrders($r_id){
        $orders = $this->entityManager
            ->getRepository(Order::class)
//            ->find();
            ->findBy(array('restaurant'=>$r_id));
        if(!$orders){
            throw new \Exception("Orders not found", 404);
        }
        return $orders;
    }
}
