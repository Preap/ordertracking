<?php
/**
 * Created by PhpStorm.
 * User: CUTing Edge
 * Date: 12/03/2019
 * Time: 5:02 PM
 */

namespace App\Service;


use App\Entity\Driver;
use App\Entity\Orders;
use App\Entity\Vehicle;
use App\Request\AssignOrderRequest;
use App\Request\ChangeOrderStatusRequest;
use App\Response\OrdersResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class OrdersService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $r_id
     * @return array|object[]
     * @throws \Exception
     */
    public function getOrders($r_id){
        $orders = $this->entityManager
            ->getRepository(Orders::class)
            ->findBy(array('restaurant'=>$r_id));

        if(!$orders){
            throw new \Exception("Orders not found!",404);
        }
        return $orders;
    }

    public function assignOrder(AssignOrderRequest $request)
    {   $order = new Orders();

        // get Driver object from DB
        $driver = $this->entityManager
            ->getRepository(Driver::class)
            ->find($request->driver_id);

        // get Vehicle object from DB
        $vehicle = $this->entityManager
            ->getRepository(Vehicle::class)
            ->find($request->vehicle_id);

        $order = $this->entityManager
            ->getReference(Orders::class,$request->order_id);
        $order->setDriver($driver);
        $order->setVehicle($vehicle);
        $this->entityManager->flush();

        return new OrdersResponse(
            "Order with ID: " .$request->order_id.
            " successfully updated");
    }

    /**
     * @param $driver_id
     * @return array|object[]
     * @throws \Exception
     */
    public function getDeliveryList($driver_id) {
        $orders = $this->entityManager
            ->getRepository(Orders::class)
            ->findBy(array('driver'=>$driver_id,
                            'type'=>'delivery'));

        if(!$orders){
            throw new \Exception("Delivery List empty!",404);
        }
        return $orders;
    }

    public function changeOrderStatus(ChangeOrderStatusRequest $request) {
        $order = $this->entityManager->getReference(Orders::class,$request->order_id);
        $order->setStatus($request->status);
        $this->entityManager->flush();

        return new OrdersResponse("Order with ID: " .$request->order_id." successfully updated with status: ". $request->status);
    }

}
