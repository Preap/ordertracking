<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 16:30
 */

namespace App\Service;


use App\Request\HomepageRequest;
use App\Response\HomepageResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class HomepageService
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * HomepageService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function homepage(HomepageRequest $request){
        $responseMessage = $request->name. " is " . $request->age . " years old and he is " . $request->description;
// Here we load from DB, call methods and persist/flush
        return new HomepageResponse($responseMessage);
    }
}
