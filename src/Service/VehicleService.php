<?php
/**
 * Created by PhpStorm.
 * User: A_Lambov
 * Date: 12-Mar-19
 * Time: 10:54
 */

namespace App\Service;


use App\Entity\Vehicle;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class VehicleService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $r_id
     * @return array|object[]
     * @throws \Exception
     */
    public function getVehicles($r_id){
        $vehicles = $this->entityManager
            ->getRepository(Vehicle::class)
            ->findBy(array('restaurant'=>$r_id));

        if(!$vehicles){
            throw new \Exception("Vehicles not found", 404);
        }
        return $vehicles;
    }
}
