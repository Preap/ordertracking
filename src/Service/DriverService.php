<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 23:00
 */

namespace App\Service;


use App\Entity\Driver;
use App\Request\ChangeDriverAvailabilityRequest;
use App\Request\CheckDriverCredentialsRequest;
use App\Response\DriverResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DriverService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $r_id
     * @return array|object[]
     * @throws \Exception
     */
    public function getDrivers($r_id){
        $drivers = $this->entityManager
            ->getRepository(Driver::class)
            ->findBy(array('restaurant'=>$r_id));
//            ->findAll();
        if(!$drivers){
            throw new \Exception("Drivers not found", 404);
        }
        return $drivers;
    }

    /**
     * @param CheckDriverCredentialsRequest $request
     * @return array|object[]
     * @throws \Exception
     */
    public function checkCredentials(CheckDriverCredentialsRequest $request)
    {
        $driver = $this->entityManager
            ->getRepository(Driver::class)
            ->findBy(array(
                'phone'=>$request->phone,
                'password'=>$request->password));

        if(!$driver){
            throw new \Exception("No driver with such credentials found!", 404);
        }
        return $driver;
    }

    public function getDriver($driver_id) {
        $driver = $this->entityManager
            ->getRepository(Driver::class)
            ->find($driver_id);

        if(!$driver){
            throw new \Exception('Driver not found!', 404);
        }

        return $driver;
    }

    public function changeDriverAvailability(ChangeDriverAvailabilityRequest $request){
        $driver = $this->entityManager->getReference(Driver::class, $request->driver_id);
        $driver->setAvailability($request->availability);
        $this->entityManager->flush();

        return new DriverResponse("Driver with ID: " .$request->driver_id. " succesfully update to availability: ". $request->availability);
    }

}
