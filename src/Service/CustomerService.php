<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 23-Feb-19
 * Time: 03:38
 */

namespace App\Service;


use App\Entity\Customer;
use App\Request\CustomerRequest;
use App\Response\CustomerResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class CustomerService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param CustomerRequest $requestedCustomer
     * @return CustomerResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function insertCustomer(CustomerRequest $requestedCustomer)
    {
        $customer = new Customer();

        $customer->setFirstName($requestedCustomer->first_name);
        $customer->setLastName($requestedCustomer->last_name);
        $customer->setEmail($requestedCustomer->email);
        $customer->setPhone($requestedCustomer->phone);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        $responseMessage = ("Successfully added new customer with ID: ".$customer->getId()." to the database!");
        return new CustomerResponse($responseMessage);
    }

    /**
     * @param $id
     * @return object|null
     * @throws \Exception
     */
    public function getCustomer($id){
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->find($id);

        if(!$customer){
            throw new \Exception("Customer not found",404);
        }
        return $customer;
    }

}
