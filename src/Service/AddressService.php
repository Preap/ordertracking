<?php
/**
 * Created by PhpStorm.
 * User: Nasko
 * Date: 22-Feb-19
 * Time: 18:05
 */

namespace App\Service;


use App\Entity\Address;
use App\Request\AddressRequest;
use App\Response\AddressResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class AddressService
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * AddressService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param AddressRequest $request
     * @return AddressResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function insertAddress(AddressRequest $request)
    {
        $address = new Address();
        $address->setArea($request->area);
        $address->setLatitude($request->latitude);
        $address->setLongitude($request->longitude);
        $address->setNumber($request->number);
        $address->setPostCode($request->post_code);
        $address->setStreetName($request->street_name);

        $this->entityManager->persist($address);
        $this->entityManager->flush();

//        $responseMessage = ("Area: ".$request->area." ".$request->post_code." ".$request->street_name." ".$request->number." ".$request->latitude." ".$request->longitude);
        $responseMessage = ("Successfully added new address with ID: ".$address->getId()." to the database!");
        return new AddressResponse($responseMessage);
    }

    /**
     * @param $id
     * @return object|null
     * @throws \Exception
     */
    public function getAddress($id){
        $address = $this->entityManager
            ->getRepository(Address::class)
            ->find($id);

        if(!$address){
            throw new \Exception("Address not found",404);
        }
        return $address;
    }

}
